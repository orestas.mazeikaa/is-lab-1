import random
import numpy
import sys
import os

log_file_name = 'log.txt'
data_file_name = 'data.txt'
final_answer_file_name = 'final_answer.txt'

LEARNING_STEP = .5


def get_lines():
    data = open(data_file_name)
    return data.readlines()


def get_vars_from_line(line):
    split_line = line.split(',')
    return float(split_line[0]), float(split_line[1]), float(split_line[2])


def get_file_line_count():
    return len(open(data_file_name).readlines())


def learn(w, eta, fault, x=1):
    return w + eta * fault * x


def get_random_number():
    return numpy.random.randn(1)


def print_to_log(line, iterations, fault, w1, w2, b, result, d):
    log = open(log_file_name, "a+")
    log.writelines('---------Data---------\n')
    log.writelines('{}\n'.format(line.rstrip('\n')))
    log.writelines('----------------------\n')
    log.writelines('Iterations: {}\n'.format(iterations))
    log.writelines('Fault: {}\n'.format(fault))
    log.writelines('W1: {}\n'.format(w1))
    log.writelines('W2: {}\n'.format(w2))
    log.writelines('B: {}\n'.format(b))
    log.writelines('Actual result: {}\n'.format(result))
    log.writelines('Expected result: {}\n'.format(d))
    log.writelines('----------------------\n')
    log.writelines('\n')
    log.writelines('\n')
    log.close()


def print_final_answer(w1, w2, b, iterations, fault):
    log = open(final_answer_file_name, "w")
    log.writelines('Found in {} iterations.\n'.format(iterations))
    log.writelines('W1: {}\n'.format(w1))
    log.writelines('W2: {}\n'.format(w2))
    log.writelines('B: {}\n'.format(b))
    log.writelines('Is answer correct: {}\n'.format('True' if fault == 0 else 'False'))
    log.close()


def test_if_results_are_correct(w1, w2, b):
    lines = get_lines()

    for line in lines:
        x1, x2, d = get_vars_from_line(line)
        output = x1 * w1 + x2 * w2 + b
        if output > 0:
            result = 1
        if output <= 0:
            result = -1


def delete_file_if_exists(file_name):
    if os.path.exists(file_name):
        os.remove(file_name)


def calculate():
    delete_file_if_exists(log_file_name)
    lines = get_lines()

    w1 = get_random_number()
    w2 = get_random_number()
    b = get_random_number()
    eta = LEARNING_STEP

    iterations = 0
    fault = None

    while iterations < 100000 and fault != 0:
        for line in lines:
            x1, x2, d = get_vars_from_line(line)
            output = x1 * w1 + x2 * w2 + b

            if output > 0:
                result = 1
            if output <= 0:
                result = -1

            fault = d - result

            if fault != 0:
                w1 = learn(w1, eta, fault, x1)
                w2 = learn(w2, eta, fault, x2)
                b = learn(b, eta, fault)
                break
            iterations = iterations + 1
        print_to_log(line, iterations, fault, w1, w2, b, result, d)

    test_if_results_are_correct(w1, w2, b)
    print_final_answer(w1, w2, b, iterations, fault)


calculate()
